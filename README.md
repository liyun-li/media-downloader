# Media Downloader

A JavaScript Bookmarklet that detects and downloads media contents from a web page. It does not work on streaming websites. You probably want to check out `youtube-dl` for downloading videos from YouTube.

## Usage

Simply bookmark the content of `output.txt`.

## Contribution

Node 12+ and NPM 6+ are the only true dependencies so install them first.

To install the rest:

```sh
npm install
```

To update `output.txt`:

```sh
npm start
```

## Acknowledgement

* [Minify](https://github.com/coderaiser/minify)
* [W3 CSS](https://www.w3schools.com/w3css)
