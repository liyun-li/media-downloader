const fs = require('fs')
const minify = require('minify')

// escape backticks
const rback = s => '`' + s.replace(/`/g, '\`') + '`'

const stylize = css => `<style>${css}</style>`

const main = async () => {
  const index = await minify('index.js')
  const ui = await minify('ui.html')
  const cssMaterial = await minify('style.css')

  const html = '`' + (stylize(cssMaterial) + ui).replace(/`/g, '\`') + '`'

  // do not change the order
  const main = index.replace('__UI_PLACEHOLDER__', html)

  fs.writeFile('output.txt', `javascript:${main}\n`, error => {
    if (error) throw error
    console.log('OK!')
  })

}

main()
