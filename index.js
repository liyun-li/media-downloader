(HTML => {
  const gebi = i => document.getElementById(i)
  const gebc = c => document.getElementsByClassName(c)
  const dce = e => document.createElement(e)

  if (window.mediaDownloaderByLiyun) {
    window.mediaDownloaderByLiyun.destroy()
    delete window.mediaDownloaderByLiyun
  }

  const IMAGE_FORMATS = {
    'apng': 1,
    'avif': 1,
    'bmp': 1,
    'gif': 1,
    'ico': 1,
    'jpg': 1,
    'jpeg': 1,
    'jpe': 1,
    'jif': 1,
    'jfif': 1,
    'png': 1,
    'svg': 1,
    'tif': 1,
    'tiff': 1,
    'webp': 1,
    'xbm': 1
  }

  const AUDIO_FORMATS = {
    'aac': 1,
    'flac': 1,
    'mp3': 1,
    'm4p': 1,
    'oga': 1,
    'wav': 1
  }

  const VIDEO_FORMATS = {
    '3gp': 1,
    'mpg': 1,
    'mpeg': 1,
    'mp4': 1,
    'm4a': 1,
    'm4v': 1,
    'mov': 1,
    'ogg': 1,
    'ogv': 1,
    'webm': 1
  }

  const getFileName = url => {
    // TODO: work on using a single replace()
    // PROGRESS: url.replace(/^.*\/([^?#]*)\.([^?#]+).*$/, '$2')
    // EDGE CASE: https://10.0.0.1/asdf?#bob -> 1/asdf
    return url.replace(/^.*[/]/, '').replace(/\?.*/, '').replace(/#.*/, '')
  }


  const getMediaType = fileName => {
    if (!fileName || !fileName.includes('.')) return ''

    const extension = fileName.split('.').slice(-1)[0]
    if (IMAGE_FORMATS[extension]) return 'image'
    else if (AUDIO_FORMATS[extension]) return 'audio'
    else if (VIDEO_FORMATS[extension]) return 'video'

    return ''
  }

  const getAllLinks = () => {
    const links = {}
    const all = document.querySelectorAll('*')

    for (const e of all) {
      if (e.src) {
        links[e.src] = e
      }

      if (e.href) {
        links[e.href] = e
      }
    }

    return links
  }

  class MediaDownloader {
    loadingCounter = 0

    ui = dce('div')

    constructor() {
      this.ui.innerHTML = HTML

      document.body.append(this.ui)

      this.displayResult(false)
      this.loadingInterval = setInterval(this.setLoading, 100)

      const table = gebi('mdbl-table')
      const links = getAllLinks()
      for (const url in links) {
        const fileName = getFileName(url)
        const mediaType = getMediaType(fileName)

        if (mediaType || ['IMG', 'SOURCE', 'VIDEO', 'AUDIO', 'SVG'].includes(links[url].tagName)) {
          const ul = dce('ul')
          const data = dce('a')

          data.href = url
          data.download = fileName
          data.textContent = fileName
          ul.className = 'w3-ul'
          ul.style.paddingLeft = '1.2vw'
          ul.append(data)

          ul.onmouseover = () => {
            let node

            if (links[url].tagName === 'LINK') {
              node = dce('img')
              node.src = url
            } else node = links[url].cloneNode(true)

            node.style.height = 'auto'
            node.style.width = 'auto'
            node.style.maxHeight = '100%'
            node.style.maxWidth = '100%'
            gebi('mdbl-preview-content').append(node)
          }

          ul.onmouseout = () => {
            gebi('mdbl-preview-content').innerHTML = ''
          }

          gebi('mdbl-table-all').append(ul)

          if (mediaType) gebi(`mdbl-table-${mediaType}`).append(ul.cloneNode(true))
          else {
            switch (links[url].tagName) {
              case 'LINK':
              case 'IMG':
              case 'SVG':
                gebi('mdbl-table-image').append(ul.cloneNode(true))
                break
              case 'VIDEO':
                gebi('mdbl-table-video').append(ul.cloneNode(true))
                break
              case 'AUDIO':
                gebi('mdbl-table-audio').append(ul.cloneNode(true))
                break
              default:
                break
            }
          }
        }
      }

      this.filterSetup()
      this.clearLoading()
    }

    filterSetup = () => {
      for (const e of gebc('tab')) {
        e.onclick = () => {
          for (const f of gebc('mdbl-filter')) f.style.display = 'none'
          gebi(e.id.replace('tab', 'table')).style.display = ''
        }
      }
    }

    displayResult = display => gebi('mdbl-root').style.display = display ? '' : 'none'

    clearLoading = () => {
      gebi('mdbl-loading').style.display = 'none'
      clearInterval(this.loadingInterval)
      this.displayResult(true)
    }

    setLoading = () => {
      if (this.loadingCounter > 6) this.loadingCounter = 1
      const text = 'Loading' + '.'.repeat(this.loadingCounter++)
      gebi('mdbl-loading').textContent = text
    }

    destroy = () => {
      this.clearLoading()
      this.ui.remove()
    }
  }


  window.mediaDownloaderByLiyun = new MediaDownloader
  gebi('mdbl-tab-close').onclick = () => {
    window.mediaDownloaderByLiyun.destroy()
    delete window.mediaDownloaderByLiyun
  }

})(__UI_PLACEHOLDER__, true)
